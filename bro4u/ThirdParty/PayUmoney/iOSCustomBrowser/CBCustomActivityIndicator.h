//
//  CustomActivityIndicator.h
//  CustomActivityIndicator
//
//  Created by Suryakant Sharma on 11/05/15.
//  Copyright (c) 2015 PayU, India. All rights reserved.
//

/*!
 * This class is used to show PayU Activity indicator.
 */
#import <UIKit/UIKit.h>

@interface CBCustomActivityIndicator : UIView

@end
