//
//  PayUMoneyViewController.h
//  bro4u
//
//  Created by Rahul on 05/04/16.
//  Copyright © 2016 AppLearn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayUMoneyViewController : UIViewController

@property (nonatomic, strong) NSString *paymentType;
@property (nonatomic, strong) NSString *selectedBankCode;
@property (nonatomic, strong) NSString *cardNumber;
@property (nonatomic, strong) NSString *cardExpYear;
@property (nonatomic, strong) NSString *cardExpMonth;
@property (nonatomic, strong) NSString *nameOnCard;
@property (nonatomic, strong) NSString *cardNo;
@property (nonatomic, strong) NSString *CVVNo;



@end
