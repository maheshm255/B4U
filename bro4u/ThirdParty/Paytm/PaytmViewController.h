////
////  PaytmViewController.h
////  SegmentControlDemo
////
////  Created by Rahul on 27/02/16.
////  Copyright © 2016 MSP-User3. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "PaymentsSDK.h"
//
//
//@interface PaytmViewController : UIViewController<PGTransactionDelegate>
//
//@property (nonatomic, strong) PGMerchantConfiguration *mc;
//@property (nonatomic, strong) NSMutableDictionary *orderDict;
//
//+(NSString*)generateOrderIDWithPrefix:(NSString *)prefix;
//-(void)createOrder;
//@end
