//
//  RadioButtonExtension.swift
//  bro4u
//
//  Created by MACBookPro on 4/18/16.
//  Copyright © 2016 AppLearn. All rights reserved.
//

import UIKit

class RadioButtonExtension: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    // Images
    let checkedImage = UIImage(named: "radioBlue")! as UIImage
    let uncheckedImage = UIImage(named: "radioGray")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, forState: .Normal)
            } else {
                self.setImage(uncheckedImage, forState: .Normal)
            }
        }
    }
    
    override func awakeFromNib() {
        self.addTarget(self, action: "buttonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.isChecked = false
    }
    
    func buttonClicked(sender: UIButton) {
        if sender == self {
            if isChecked == true {
                isChecked = false
            } else {
                isChecked = true
            }
        }
    }


}
