//
//  b4u-ExpandableTblViewCell.swift
//  bro4u
//
//  Created by Tools Team India on 22/02/16.
//  Copyright © 2016 AppLearn. All rights reserved.
//

import UIKit

class b4u_ExpandableTblViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnPlus: b4u_Button!
    @IBOutlet weak var btnMinus: b4u_Button!
    @IBOutlet weak var iconImgView: UIImageView!
}
