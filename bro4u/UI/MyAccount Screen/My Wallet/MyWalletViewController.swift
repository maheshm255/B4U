//
//  MyWalletViewController.swift
//  ThanksScreen
//
//  Created by MSP-User3 on 03/03/16.
//  Copyright © 2016 MSP-User3. All rights reserved.
//

import UIKit

class MyWalletViewController: UIViewController ,UITextFieldDelegate {

  @IBOutlet var couponcodeTxtFld: UITextField!
  @IBOutlet var walletImageView: UIImageView!
  @IBOutlet var walletMoneyLbl: UILabel!
  
  @IBOutlet weak var walletBalanceTableView: UITableView!

  var myModelArr:[b4u_MyWalletModel] = Array()
  var indicatorcolor:UIView!

 
  
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.couponcodeTxtFld.delegate = self
//        // Do any additional setup after loading the view.
//      self.addLoadingIndicator()

        self.getData()
      
//      NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
//      NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)

      
      }
    
//    func getData()
//    {
//      b4u_Utility.sharedInstance.activityIndicator.startAnimating()
//
//        var user_id = ""
//        
//        if let loginInfoData:b4u_LoginInfo = bro4u_DataManager.sharedInstance.loginInfo{
//            
//            user_id = loginInfoData.userId! //Need to use later
//            
//        }
//        
//        //user_id = "1"
//        
//        
//        let params = "?user_id=\(user_id)&\(kAppendURLWithApiToken)"
//        b4u_WebApiCallManager.sharedInstance.getApiCall(kMyWalletIndex , params:params, result:{(resultObject) -> Void in
//            
//            print(" Wallet Balance Data Received")
//            
//            print(resultObject)
//            
//            b4u_Utility.sharedInstance.activityIndicator.stopAnimating()
//            
//            self.congigureUI()
//            
//        })
//        
//        
//   }

  
    func getData()
    {
      //2. Checking for Network reachability
      
      if(AFNetworkReachabilityManager.sharedManager().reachable){
        
        self.couponcodeTxtFld.delegate = self
        // Do any additional setup after loading the view.
        self.addLoadingIndicator()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)

        b4u_Utility.sharedInstance.activityIndicator.startAnimating()
        
        var user_id = ""
        
        if let loginInfoData:b4u_LoginInfo = bro4u_DataManager.sharedInstance.loginInfo{
          
          user_id = loginInfoData.userId! //Need to use later
          
        }
        
        //user_id = "1"
        
        
        let params = "?user_id=\(user_id)&\(kAppendURLWithApiToken)"
        b4u_WebApiCallManager.sharedInstance.getApiCall(kMyWalletIndex , params:params, result:{(resultObject) -> Void in
          
          print(" Wallet Balance Data Received")
          
          print(resultObject)
          
          b4u_Utility.sharedInstance.activityIndicator.stopAnimating()
          
          self.congigureUI()
          
        })
        //3.Remove observer if any remain
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "NoNetworkConnectionNotification", object: nil)
        
      }else{
        //4. First Remove any existing Observer
        //Add Observer for No network Connection
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "NoNetworkConnectionNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyWalletViewController.getData), name: "NoNetworkConnectionNotification", object: nil)
        
        //5.Adding View for Retry
        let noNetworkView = NoNetworkConnectionView(frame: CGRectMake(0,0,self.view.frame.width,self.view.frame.height))
        self.view.addSubview(noNetworkView)
        
        return
      }
    }
  
  
    func congigureUI()
    {
       walletBalanceTableView .reloadData()
      
        if let walletBalance =   bro4u_DataManager.sharedInstance.walletBalanceData
        {
            self.walletMoneyLbl.text = "\(walletBalance)"
        }
    }
  
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return bro4u_DataManager.sharedInstance.myWalletData.count
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    let textCellIdentifier = "MyWalletTableViewCellID"
    let cell = tableView.dequeueReusableCellWithIdentifier(textCellIdentifier, forIndexPath: indexPath) as! b4u_MyWalletTableViewCell
    
    let myWalletModel:b4u_MyWalletModel = bro4u_DataManager.sharedInstance.myWalletData[indexPath.section]
    
    cell.configureData(myWalletModel)
    
    b4u_Utility.shadowEffectToView(cell)

    return cell
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 95.0
    }
  
//  @IBAction func cancelBtnClicked(sender: AnyObject) {
//    self.dismissViewControllerAnimated(true, completion:nil)
//    }
    
    @IBAction func applyBtnAction(sender: AnyObject)
    {
     
        
        guard let referelCode = couponcodeTxtFld.text where referelCode != ""else
        {
            self.view.makeToast(message:"Please enter coupon code", duration:1.0, position:HRToastPositionDefault)
            return
        }
        
        b4u_Utility.sharedInstance.activityIndicator.startAnimating()
        
        var user_id = ""
        
        if let loginInfoData:b4u_LoginInfo = bro4u_DataManager.sharedInstance.loginInfo{
            
            user_id = loginInfoData.userId! //Need to use later
            
        }
        
        //user_id = "1"
        
        
        let params = "?user_id=\(user_id)&device_id=\(b4u_Utility.getUUIDFromVendorIdentifier())&referral_code=\(referelCode)&\(kAppendURLWithApiToken)"
        
        b4u_WebApiCallManager.sharedInstance.getApiCall(kApplyWalletCouponIndex , params:params, result:{(resultObject) -> Void in
            
              b4u_Utility.sharedInstance.activityIndicator.stopAnimating()

              self.applyReferralCode(resultObject as! String)
            
               print(resultObject)

            
//               if resultObject as! String == "Success"
//               {
//                  self.view.makeToast(message:"Referrel code applied successfully", duration:1.0, position:HRToastPositionDefault)
//               }else
//               {
//                self.view.makeToast(message:"Please enter valid code", duration:1.0, position:HRToastPositionDefault)
//               }
            
        })
        
    }
    func addLoadingIndicator () {
    self.view.addSubview(b4u_Utility.sharedInstance.activityIndicator)
    self.view.bringSubviewToFront(b4u_Utility.sharedInstance.activityIndicator)
    b4u_Utility.sharedInstance.activityIndicator.center = self.view.center
  }

    
    func applyReferralCode(resultObject:String)
    {
        let codeValidateStatus = bro4u_DataManager.sharedInstance.walletCouponCodeStatus
        let codeValidateMessage = bro4u_DataManager.sharedInstance.walletCouponCodeMessage
        
        if resultObject == "Success" && codeValidateStatus == "true"
        {
//            delegate?.couponApplied(self.couponcodeTxtFld.text!)
            self.view.makeToast(message:codeValidateMessage!, duration:1.0, position:HRToastPositionDefault)
            self.couponcodeTxtFld.text = ""
        }
        else
        {
            self.view.makeToast(message:codeValidateMessage!, duration:1.0, position:HRToastPositionDefault)
            self.couponcodeTxtFld.text = ""
        }
        
        
//        if let orderDetailModel = bro4u_DataManager.sharedInstance.orderDetailData.first
//        {
//            if let selectionLocal: b4u_SelectionModel =  orderDetailModel.selection?.first{
//                self.lblAmount.text = "  Rs. \(selectionLocal.grandTotal!).00"
//            }
//        }
        self.view.endEditing(true)
        
    }

  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
    if section == 0
    {
      return 1.0;
    }
    else
    {
      return 10.0;
    }
    
  }


    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
  
  /**
   * Called when the user click on the view (outside the UITextField).
   */
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    self.view.endEditing(true)
  }

  
  //To move view Up when Keyboard Appears
  
  func keyboardWillShow(notification: NSNotification) {
    
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
      self.view.frame.origin.y -= keyboardSize.height - 30
    }
    
  }
  
  func keyboardWillHide(notification: NSNotification) {
    if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
      self.view.frame.origin.y += keyboardSize.height - 30
    }
  }


}
